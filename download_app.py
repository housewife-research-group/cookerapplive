#Written by: Neyol Dilina, 23-May-2015
#Downloads a fresh copy of the Cooker App from the internet.
#File will be saved in the location where this script going to be

import urllib2

URL = 'https://bitbucket.org/housewife-research-group/cookerapplive/downloads/CookerRPi.jar'
try:
    with open('CookerRPi.jar','wb') as f:
        f.write(urllib2.urlopen(URL).read())
        f.close()
    print "Download Complete!"
except:
    print "Network error!"
    

